variable "name" {
  type = string
  default = "neo-network-access"
}

variable "principal_arns" {
  type = list
  default = ["arn:aws:iam::223431100518:role/neo-customer-network-access"]
}
