data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "cross-account-assume-role-policy" {
  statement {
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = var.principal_arns
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "NetworkEngineeringOperations-tgw-access-policy" {
  statement {
    sid    = "ResourcespecificWriteAccess"
    effect = "Allow"

    actions = [
        "ec2:CreateRoute",
        "ec2:DeleteRoute",
        "ec2:ReplaceRoute",
        "ec2:DeleteTransitGateway",
        "ec2:CreateTransitGatewayVpcAttachment",
        "ec2:DeleteTransitGatewayVpcAttachment",
        "ec2:ModifyTransitGatewayVpcAttachment",
        "ec2:AcceptTransitGatewayVpcAttachment",
        "ec2:RejectTransitGatewayVpcAttachment",
        "ram:TagResource",
        "ram:UntagResource",
        "ram:DisassociateResourceShare",
        "ram:AcceptResourceShareInvitation",
        "ram:RejectResourceShareInvitation",
        "ram:ListPendingInvitationResources"
    ]

    resources = [
        "arn:aws:ec2:*:*:transit-gateway/*",
        "arn:aws:ec2:*:${data.aws_caller_identity.current.account_id}:vpc/*",
        "arn:aws:ec2:*:${data.aws_caller_identity.current.account_id}:subnet/*",
        "arn:aws:ec2:*:${data.aws_caller_identity.current.account_id}:route-table/*",
        "arn:aws:ec2:*:${data.aws_caller_identity.current.account_id}:transit-gateway-attachment/*",
        "arn:aws:ram:*:223431100518:resource-share/*",
        "arn:aws:ram:*:223431100518:resource-share-invitation/*"
    ]
  }

  statement {
    sid    = "AllGetResources"
    effect = "Allow"

    actions = [
        "ec2:DescribeVpcs",
        "ec2:DescribeSubnets",
        "ec2:DescribeRouteTables",
        "ec2:DescribeVpcAttribute",
        "ec2:ModifySubnetAttribute",
        "ec2:DescribeTransitGateways",
        "ec2:DescribeAvailabilityZones",
        "ec2:DescribeTransitGatewayAttachments",
        "ec2:DescribeTransitGatewayVpcAttachments",
        "ec2:GetTransitGatewayAttachmentPropagations",
        "ram:ListResources",
        "ram:ListPrincipals",
        "ram:ListPermissions",
        "ram:GetResourceShares",
        "ram:GetResourceShareInvitations",
        "ram:GetResourceShareAssociations",
        "ram:ListResourceSharePermissions",
        "ram:ListPendingInvitationResources"
    ]

    resources = ["*"]
  }

  statement {
    sid    = "TransitGWTagging"
    effect = "Allow"

    actions = [
        "ec2:DeleteTags",
        "ec2:CreateTags"
    ]

    resources =[
        "arn:aws:ec2:*:*:transit-gateway/*",
        "arn:aws:ec2:*:${data.aws_caller_identity.current.account_id}:transit-gateway-attachment/*"
    ]
  }
  
  statement {
    sid    = "TransitGWServiceRolePermissions"
    effect = "Allow"

    actions = [
        "iam:CreateServiceLinkedRole"
    ]
    
    resources = [
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/transitgateway.amazonaws.com/AWSServiceRoleForVPCTransitGateway*"
    ]

    condition {
	test = "StringLike"
	variable = "iam:AWSServiceName"
	values = ["transitgateway.amazonaws.com"]
    }
  }

  statement {
    sid    = "TransitGWServicePolicyPermissions"
    effect = "Allow"

    actions = [
        "iam:AttachRolePolicy",
        "iam:PutRolePolicy"
    ]
    
    resources = [
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/transitgateway.amazonaws.com/AWSServiceRoleForVPCTransitGateway*"
    ]
  }
}

resource "aws_iam_role" "cross-account-assume-role" {
  name               = var.name
  assume_role_policy = data.aws_iam_policy_document.cross-account-assume-role-policy.json
}

resource "aws_iam_policy" "NetworkEngineeringOperations-tgw-access" {
  name               = "NetworkEngineeringOperations-tgw-access"
  policy             = data.aws_iam_policy_document.NetworkEngineeringOperations-tgw-access-policy.json
}

resource "aws_iam_role_policy_attachment" "cross-account-network-access" {
 role                = aws_iam_role.cross-account-assume-role.name
 policy_arn          = aws_iam_policy.NetworkEngineeringOperations-tgw-access.arn
}
