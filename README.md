## Commands

**Creating a NEO-Network-Access Role**

This manifest will create an IAM role for Network Engineering Operations team in your account. Principals authorized will use STS to assume role into customers account to perform necessary actions like:
- accepting Transit Gateway(TGW) resource share in the customers account
- creating TGW VPC attachment and tagging it
- creating routes in the route tables, that are allowed on TGW

If you are running this manifest seperately

```bash
git clone "git@code.vt.edu:it-common-platform/support/terraform/aws-transitgw-role-creator.git"
cd aws-transitgw-role-creator/
export AWS_PROFILE=your-aws-profile
terraform init
terraform plan
terraform apply
```

If you are running this as part of the infrastructure manifest, you can call this as a module

```bash
module "iam-network-team-access" {
  source  = "git::https://code.vt.edu/it-common-platform/support/terraform/aws-transitgw-role-creator.git"
}
```

You should see a terraform plan similar to this:

```bash
An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_iam_policy.NetworkEngineeringOperations-tgw-access will be created
  + resource "aws_iam_policy" "NetworkEngineeringOperations-tgw-access" {
      + arn    = (known after apply)
      + id     = (known after apply)
      + name   = "NetworkEngineeringOperations-tgw-access"
      + path   = "/"
      + policy = jsonencode(
            {
              + Statement = [
                  + {
                      + Action   = [
                          + "ram:UntagResource",
                          + "ram:TagResource",
                          + "ram:RejectResourceShareInvitation",
                          + "ram:ListPendingInvitationResources",
                          + "ram:DisassociateResourceShare",
                          + "ram:AcceptResourceShareInvitation",
                          + "ec2:ReplaceRoute",
                          + "ec2:RejectTransitGatewayVpcAttachment",
                          + "ec2:ModifyTransitGatewayVpcAttachment",
                          + "ec2:DeleteTransitGatewayVpcAttachment",
                          + "ec2:DeleteTransitGateway",
                          + "ec2:DeleteRoute",
                          + "ec2:CreateTransitGatewayVpcAttachment",
                          + "ec2:CreateRoute",
                          + "ec2:AcceptTransitGatewayVpcAttachment",
                        ]
                      + Effect   = "Allow"
                      + Resource = [
                          + "arn:aws:ram:*:223431100518:resource-share/*",
                          + "arn:aws:ram:*:223431100518:resource-share-invitation/*",
                          + "arn:aws:ec2:*:553812247722:vpc/*",
                          + "arn:aws:ec2:*:553812247722:transit-gateway-attachment/*",
                          + "arn:aws:ec2:*:553812247722:subnet/*",
                          + "arn:aws:ec2:*:553812247722:route-table/*",
                          + "arn:aws:ec2:*:*:transit-gateway/*",
                        ]
                      + Sid      = "ResourcespecificWriteAccess"
                    },
                  + {
                      + Action   = [
                          + "ram:ListResources",
                          + "ram:ListResourceSharePermissions",
                          + "ram:ListPrincipals",
                          + "ram:ListPermissions",
                          + "ram:ListPendingInvitationResources",
                          + "ram:GetResourceShares",
                          + "ram:GetResourceShareInvitations",
                          + "ram:GetResourceShareAssociations",
                          + "ec2:ModifySubnetAttribute",
                          + "ec2:GetTransitGatewayAttachmentPropagations",
                          + "ec2:DescribeVpcs",
                          + "ec2:DescribeVpcAttribute",
                          + "ec2:DescribeTransitGateways",
                          + "ec2:DescribeTransitGatewayAttachments",
                          + "ec2:DescribeTransitGatewayVpcAttachments",
                          + "ec2:DescribeSubnets",
                          + "ec2:DescribeRouteTables",
                          + "ec2:DescribeAvailabilityZones",
                        ]
                      + Effect   = "Allow"
                      + Resource = "*"
                      + Sid      = "AllGetResources"
                    },
                  + {
                      + Action   = [
                          + "ec2:DeleteTags",
                          + "ec2:CreateTags",
                        ]
                      + Effect   = "Allow"
                      + Resource = [
                          + "arn:aws:ec2:*:553812247722:transit-gateway-attachment/*",
                          + "arn:aws:ec2:*:*:transit-gateway/*",
                        ]
                      + Sid      = "TransitGWTagging"
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
    }

  # aws_iam_role.cross-account-assume-role will be created
  + resource "aws_iam_role" "cross-account-assume-role" {
      + arn                   = (known after apply)
      + assume_role_policy    = jsonencode(
            {
              + Statement = [
                  + {
                      + Action    = "sts:AssumeRole"
                      + Effect    = "Allow"
                      + Principal = {
                          + AWS = "arn:aws:iam::223431100518:role/neo-customer-network-access"
                        }
                      + Sid       = ""
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + create_date           = (known after apply)
      + force_detach_policies = false
      + id                    = (known after apply)
      + max_session_duration  = 3600
      + name                  = "neo-network-access"
      + path                  = "/"
      + unique_id             = (known after apply)
    }

  # aws_iam_role_policy_attachment.cross-account-network-access will be created
  + resource "aws_iam_role_policy_attachment" "cross-account-network-access" {
      + id         = (known after apply)
      + policy_arn = (known after apply)
      + role       = "neo-network-access"
    }

Plan: 3 to add, 0 to change, 0 to destroy.
```
